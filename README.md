[![pipeline status](https://gitlab.com/MushuLeDragon/cv/badges/main/pipeline.svg)](https://gitlab.com/MushuLeDragon/cv/-/commits/main)

# Json Resume

JsonResume official website [here](https://jsonresume.org/).

Resumed documentation [here](https://github.com/rbardini/resumed) (Lightweight JSON Resume builder, no-frills alternative to resume-cli).

Themes referenced [here](https://www.npmjs.com/search?q=jsonresume-theme).

YoanDev YouTube tutorial [here](https://youtu.be/wSghMQBB-6c).

My CV [here](https://mushuledragon.gitlab.io/cv/)

## Install

```shell
npm i resumed

# Install theme https://www.npmjs.com/search?q=jsonresume-theme
npm i jsonresume-theme-mooser

# Generate Json file
npx resumed init

# Generate HTML file
npx resumed render
npx resumed render --theme mooser
```

## Run

```shell
npm intall

# Generate HTML file
npx resumed render
npx resumed render --theme mooser
```

## Install specific theme

```shell
# Install theme https://www.npmjs.com/search?q=jsonresume-theme
npm i jsonresume-theme-mytheme
npx resumed render --theme mytheme
```